(in-package #:litter-shadow-conditionals)

(defmacro or (&rest forms)
  (or-expander forms))

(defmacro and (&rest forms)
  (and-expander forms))

(defmacro when (test &body body)
  `(if ,test
       (progn ,@body)
       nil))

(defmacro unless (test &body body)
  `(if ,test
       nil
       (progn ,@body)))

(defmacro cond (&rest clauses)
  (cond-expander clauses))


(defmacro case (keyform &rest clauses)
  (case-expander keyform clauses))


(defmacro ecase (keyform &rest clauses)
  (ecase-expander keyform clauses))


(defmacro ccase (keyplace &rest clauses &environment env)
  (ccase-expander keyplace clauses env))


(defmacro typecase (keyform &rest clauses)
  (typecase-expander keyform clauses))

;;; As with ECASE, the default for ETYPECASE is to signal an error.
(defun etypecase-expander (keyform clauses)
  (let* ((variable (gensym))
         (keys (collect-e/ctypecase-keys clauses))
         (final `(error 'etypecase-type-error
                        :name 'etypecase
                        :datum ,variable
                        :expected-type '(member ,@keys))))
    `(let ((,variable ,keyform))
       ,(expand-e/ctypecase-clauses clauses variable final 'etypecase))))

(defmacro etypecase (keyform &rest clauses)
  (etypecase-expander keyform clauses))


(defmacro ctypecase (keyplace &rest clauses &environment env)
  (ctypecase-expander keyplace clauses env))

