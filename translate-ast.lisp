(cl:in-package #:litter-box)

(defgeneric translate-ast (ast env dico))

(defmethod translate-ast
    ((ast cleavir-ast:fdefinition-ast) env dico)
  `(f1:fdefinition
    ,(translate-ast (cleavir-ast:name-ast ast) env dico)
    ,env))

(defmethod translate-ast
    ((ast cleavir-ast:constant-ast) env dico)
  `',(cleavir-ast:value ast))

(defmethod translate-ast
    ((ast cleavir-ast:lexical-ast) env dico)
  (cleavir-ast:name ast))

(defmethod translate-ast
    ((ast cleavir-ast:symbol-value-ast) env dico)

  ;;symbol-value-ast is for special variable or inline symbol-value when its
  ;;a special var name-ast will always be constant-ast.
  (typecase (cleavir-ast:name-ast ast)
    (cleavir-ast:constant-ast
     `(f1:special-variable ,(translate-ast (cleavir-ast:name-ast ast) env dico)
			   ,env))
    (t
     `(symbol-value
       ,(translate-ast (cleavir-ast:name-ast ast) env dico)))))

(defmethod translate-ast
    ((ast cleavir-ast:set-symbol-value-ast) env dico)

  (typecase (cleavir-ast:name-ast ast)
    (cleavir-ast:constant-ast
     ;;symbol-value-ast is for special variable or inline symbol-value when its
     ;;a special var name-ast will always be constant-ast.
     `(setf (f1:special-variable ,(translate-ast (cleavir-ast:name-ast ast) env dico)
				 ,env
				 t)
	    ,(translate-ast (cleavir-ast:value-ast ast) env dico)))
    (t
     `(setf (symbol-value
             ,(translate-ast (cleavir-ast:name-ast ast) env dico))
            ,(translate-ast (cleavir-ast:value-ast ast) env dico)))))

(defmethod translate-ast
    ((ast cleavir-ast:call-ast) env dico)

  `(funcall
    ,(translate-ast (cleavir-ast:callee-ast ast) env dico)
    ,@(loop for argument-ast in (cleavir-ast:argument-asts ast)
            collect (translate-ast argument-ast env dico))))

(defmethod translate-ast
    ((ast cleavir-ast:function-ast) env dico)

  `(lambda ,(loop for item in (cleavir-ast:lambda-list ast)
                  collect (cond ((member item lambda-list-keywords)
                                 item)
                                ((atom item)
                                 (cleavir-ast:name item))
                                (t
                                 (mapcar #'cleavir-ast:name item))))
     ,(translate-ast (cleavir-ast:body-ast ast) env dico)))

(defmethod translate-ast
    ((ast cleavir-ast:progn-ast) env dico)
  `(progn ,@(loop for form-ast in (cleavir-ast:form-asts ast)
                  collect (translate-ast form-ast env dico))))

(defmethod translate-ast
    ((ast cleavir-ast:block-ast) env dico)
  (let ((name (gensym)))
    `(block ,name
       ,(translate-ast
         (cleavir-ast:body-ast ast)
         env
         (push (cons ast name) dico)))))

(defmethod translate-ast
    ((ast cleavir-ast:return-from-ast) env dico)
  `(return-from ,(cdr (assoc (cleavir-ast:block-ast ast) dico))
     ,(translate-ast (cleavir-ast:form-ast ast) env dico)))

(defmethod translate-ast
    ((ast cleavir-ast:setq-ast) env dico)
  `(setq ,(translate-ast (cleavir-ast:lhs-ast ast) env dico)
         ,(translate-ast (cleavir-ast:value-ast ast) env dico)))

(defmethod translate-ast
    ((ast cleavir-ast:if-ast) env dico)
  `(if ,(translate-ast (cleavir-ast:test-ast ast) env dico)
       ,(translate-ast (cleavir-ast:then-ast ast) env dico)
       ,(translate-ast (cleavir-ast:else-ast ast) env dico)))


(defmethod translate-ast
    ((ast cleavir-ast:eq-ast) env dico)
  `(eq ,(translate-ast (cleavir-ast:arg1-ast ast) env dico)
       ,(translate-ast (cleavir-ast:arg2-ast ast) env dico)
       ))
