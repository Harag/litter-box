(in-package #:asdf-user)

(defsystem #:litter-box
  :depends-on (#:ford
               #:cleavir2-ast
               #:cleavir2-cst-to-ast
	       #:eclector-concrete-syntax-tree
	       #:sicl-clos
	       )
  :serial t
  :components
  ((:file "packages")
   (:file "shadow-conditionals-support")
   (:file "shadow-conditionals")
   (:file "shadowing")
   
   (:file "cleavir-methods")
   (:file "gather-lexicals")
   (:file "translate-ast")
   (:file "box")
   
   ))
