(in-package #:litter-shadow)

(cl:defun mklist (obj)
  (cond ((null obj)
	 )
	((consp obj)
	 `(list ,@(mapcar #'mklist obj) ))
	(t (if (symbolp obj)
	       `',obj
	       obj))))
#|
;;TODO: replace function-names with a version that searches through the
;;first class environment instead.

(cl:defun function-names (fun)
  "
RETURN: A list of function names fbound to the function FUN.
NOTE:   We only scan symbols and (setf symbols).  Functions could also
        be bound to slots or to various other lisp objects.
"
  (let ((names '()))
    (dolist (pack (list-all-packages) (delete-duplicates names))
      (do-symbols (sym pack)
	
        (when (and (fboundp sym) (eql fun (symbol-function sym)))
          (push sym names))
        (let ((setter `(cl:setf ,sym)))
          
          (when (and (fboundp setter) (eql fun (fdefinition setter)))
            (push setter names)))))))

|#

(cl:defun valid-function (environment function)
  (let ((function-entry))	  
    (maphash #'(lambda (key entry)
		 (declare (ignore key))
		 (typecase function
		   (standard-generic-function                           
		    (when (f1:fboundp (ford::name entry) environment)
		    ;;  (break "poes ~S" (ford::name entry))
		      (when (or
			     (f1:function-names function environment)
			     (eql function (fdefinition
					      (ford::name entry))))
			(cl:setf function-entry entry ))))
		   (t
		    (when (f1:fboundp (ford::name entry) environment)
		      (when (eql function (ford::name entry))
			(cl:setf function-entry entry))))))
	     (ford::function-entries environment))
    function-entry))


;;Shadowing apply, funcall and eval to make sure that illegal functions are not sneaked by
;;the sandbox
(cl:defun apply (function arg &rest arguments)
  (let* ((valid-function (valid-function (f1:global-environment) function)))
    
    (unless valid-function
      (error "Illegal function ~S ~%~S" function (f1:global-environment)))
	       
    (cl:apply function arg arguments)))

(cl:defun funcall (function &rest args)
  (let* ((valid-function (valid-function  (f1:global-environment) function)))
    (unless valid-function
      (error "Illegal function ~S ~%~S" function (f1:global-environment)))
    
    (cl:apply function (first args) (rest args))))

(cl:defun eval (form)          
 (litter-box::evaluate-form form (f1:global-environment)))

(cl:defun defconstant-expander (name initial-value documentation)
  (declare (ignore documentation))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (f1:constant-variable
            ',name
            (load-time-value (f1:global-environment)))
           ,initial-value)))


(cl:defmacro defconstant (name initial-value &optional documentation)
  (defconstant-expander name initial-value documentation))


(cl:defun defvar-expander (name initial-value initial-value-p documentation)
  (declare (ignore documentation))
  (if initial-value-p
      `(progn
         (eval-when (:compile-toplevel)
           (setf (f1:special-variable
                  ',name
                  (load-time-value (f1:global-environment))
                  nil)
                 nil)
           ',name)
         (eval-when (:load-toplevel :execute)
           (unless (f1:boundp
                    ',name
                    (load-time-value (f1:global-environment)))
             (setf (f1:special-variable
                    ',name
                    (load-time-value (f1:global-environment))
                    t)
                   ,initial-value))
           ',name))
      `(eval-when (:compile-toplevel :load-toplevel :execute)
         (setf (f1:special-variable
                ',name
                (load-time-value (f1:global-environment))
                nil)
               nil)
         ',name)))

(cl:defmacro defvar
    (name &optional (initial-value nil initial-value-p) documentation)
  (defvar-expander name initial-value initial-value-p documentation))


(cl:defun defparameter-expander (env name initial-value documentation)
  (declare (ignore documentation))
  `(progn
     (eval-when (:compile-toplevel)
       (setf (f1:special-variable ',name ,env nil)
             nil))
     (eval-when (:load-toplevel :execute)
       (setf (f1:special-variable
              ',name
              (load-time-value (f1:global-environment))
              t)
             ,initial-value)
       ',name)))

(cl:defmacro defparameter
    (&environment env name initial-value &optional documentation)
  (defparameter-expander env name initial-value documentation))


(cl:defun deftype-expander (name lambda-list body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (f1:type-expander
            ',name
            (load-time-value (f1:global-environment)))
           (function ,(cleavir-code-utilities:parse-deftype 
                       name
                       lambda-list
                       body)))))

(cl:defmacro deftype (name lambda-list &body body)
  (deftype-expander name lambda-list body))


(cl:defun define-compiler-macro-expander (name lambda-list body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (f1:compiler-macro-function
            ',name
            (load-time-value (f1:global-environment)))
           (function ,(cleavir-code-utilities:parse-macro
                       name
                       lambda-list
                       body)))))

(cl:defmacro define-compiler-macro (name lambda-list &body body)
  (define-compiler-macro-expander name lambda-list body))


(cl:defmacro setf (&whole form &environment env place new-value-form &rest more-pairs)
  (cond ((null more-pairs)
	 (multiple-value-bind (variables
			       values
			       store-variables
			       writer-form
			       reader-form)
	     (f1:get-setf-expansion place env)
	   (declare (ignore reader-form))
	   `(let* ,(mapcar #'list variables values)
	      ;; Optimize a bit when there is only one store variable.
	      ,(if (= 1 (length store-variables))
		   `(let ((,(first store-variables) ,new-value-form))
		      ,writer-form)
		   `(multiple-value-bind ,store-variables
			,new-value-form
		      ,writer-form)))))
	((not (null (cdr more-pairs)))
	 `(progn (setf ,place ,new-value-form)
		 (setf ,@more-pairs)))
	(t
	 (error 'odd-number-of-arguments-to-setf :form form))))


;;TODO: put (f1:global-environment) bain macro-funxtion
(cl:defmacro defmacro (name lambda-list &body body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (f1:macro-function ',name (f1:global-environment))
           ,(cleavir-code-utilities:parse-macro name lambda-list body))))


(cl:defun defun-expander (name lambda-list body)
  (multiple-value-bind (declarations documentation forms)
      (cleavir-code-utilities:separate-function-body body)
    (let ((global-env-var (gensym))
	  (arg-type
	    (cleavir-code-utilities:lambda-list-type-specifier lambda-list)))
      `(progn
         (eval-when (:compile-toplevel)
           (let ((,global-env-var (f1:global-environment)))
             (setf (f1:function-type ',name ,global-env-var)
                   `(function ,',arg-type t))
             (setf (f1:function-lambda-list ',name ,global-env-var)
                   ',lambda-list)))
         (eval-when (:load-toplevel :execute)
	   (setf (f1:fdefinition ',name (f1:global-environment))
		 (lambda ,lambda-list
		   ,@declarations
		   ,@(if (null documentation)
			 '()
			 (list documentation))
		   (block ,(if (symbolp name) name (second name))
		     ,@forms)))
           (setf (f1:function-type ',name (f1:global-environment))
                 `(function ,',arg-type t))
           (setf (f1:function-lambda-list ',name (f1:global-environment))
                 ',lambda-list)
           ',name)))))

(cl:defmacro defun (name lambda-list &body body)
  (defun-expander name lambda-list body))
