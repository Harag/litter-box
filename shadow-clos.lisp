(cl:in-package #:shadow-clos)


(defclass class (specializer)
  ((%unique-number 
    ;; FIXME: the unique numbers should be assigned during class
    ;; finalization, and not here.
    :initform (prog1 *class-unique-number* (incf *class-unique-number*))
    :reader unique-number)
   (%name 
    :initform nil
    :initarg :name 
    ;; There is a specified function named (SETF CLASS-NAME), but it
    ;; is not an accessor.  Instead it works by calling
    ;; REINITIALIZE-INSTANCE with the new name.
    :reader class-name)
   (%direct-subclasses 
    :initform '() 
    :initarg :direct-subclasses
    :accessor class-direct-subclasses)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Class REAL-CLASS.

(defclass real-class (class)
  ((%direct-default-initargs
    :initarg :direct-default-initargs
    :initform '()
    :reader class-direct-default-initargs)
   (%documentation
    :initform nil
    :accessor documentation)
   (%precedence-list
    :initform '()
    ;; The AMOP says that CLASS-PRECEDENCE-LIST should signal an error
    ;; if the class has not been finalized.  We accomplish that effect
    ;; by defining a :BEFORE method that checks this condition, and
    ;; signals an error of the class has not been finalized.
    :reader class-precedence-list
    ;; During class finalization, we need to set the value of this
    ;; slot, so we need a writer for it, and that writer should not be
    ;; named CLASS-PRECEDENCE-LIST because that name is exported.
    ;; Furthermore, also during class finalization, once the class
    ;; precedence list has been computed and store, and we need to
    ;; compute the effective slots and the default initargs, these
    ;; last two steps need to access the precedence list.  However,
    ;; because the function CLASS-PRECEDENCE-LIST signals an error if
    ;; the class is not finalized, those last two steps can not use
    ;; it.  We therefore also need an alternative reader for this slot
    ;; (we could have used SLOT-VALUE, but we prefer a reader which is
    ;; typically faster).  Our solution is to define the ACCESSOR
    ;; named PRECEDENCE-LIST.
    :accessor precedence-list)
   ;; ALLOCATE-INSTANCE and ALLOCATE-BUILT-IN-INSTANCE access this
   ;; slot in order to determine the size of the instance to allocate.
   ;; The writer is used during class finalization.
   (%instance-size :accessor instance-size)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Class BUILT-IN-CLASS.
;;;
;;; The AMOP says that the readers CLASS-DIRECT-DEFAULT-INITARGS,
;;; CLASS-DIRECT-SLOTS, CLASS-DEFAULT-INITARGS, and CLASS-SLOTS should
;;; return the empty list for a built-in class.

(defclass built-in-class (real-class)
  ((%direct-superclasses
    :initarg :direct-superclasses
    :reader class-direct-superclasses)
   (%direct-slots
    :initform '()
    :reader class-direct-slots)
   (%finalized-p
    :initform t
    :reader class-finalized-p)
   (%default-initargs
    :initarg :default-initargs
    :accessor class-default-initargs)
   (%effective-slots
    :initform '()
    :reader class-slots)))

(defclass function (t)
  ()
  (:metaclass built-in-class))



(defclass structure-class (sicl-clos:regular-class)
  ())


(defclass structure-object (standard-object)
  ()
  (:metaclass structure-class))
