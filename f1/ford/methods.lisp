(cl:in-package #:ford)

;;; Utility function for associating a name with a function.
(cl:defun add-function-name (function function-name env)
  (pushnew function-name
           (gethash function (function-names env))
           :test #'equal))

;;; Utility function for disassociating a name from a function.
(cl:defun remove-function-name (function function-name env)
  (setf (gethash function (function-names env))
        (remove function-name (gethash function (function-names env))
                :test #'equal))
  (when (null (gethash function (function-names env)))
    (remhash function (function-names env))))

;;; Recall that this function should return true if FUNCTION-NAME has
;;; a definition in ENVIRONMENT as an ordinary function, a generic
;;; function, a macro, or a special operator.
(defmethod f1:fboundp (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (and (not (null entry))
         (or (not (eq (car (function-cell entry))
                      (unbound entry)))
             (not (null (macro-function entry)))
             (not (null (special-operator entry)))))))

(defmethod f1:fmakunbound (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (unless (null entry)
      (remove-function-name (car (function-cell entry)) function-name env)
      (setf (car (function-cell entry))
            (unbound entry))
      (setf (macro-function entry) nil)
      (setf (special-operator entry) nil)
      (setf (type entry) t)
      (setf (inline entry) nil))))

(defmethod f1:special-operator (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (if (null entry)
        nil
        (special-operator entry))))

(defmethod (cl:setf f1:special-operator)
    (new-definition function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (cond ((and (null entry) (null new-definition))
           nil)
          ((null entry)
           (cl:setf entry (ensure-function-entry env function-name))
           (cl:setf (special-operator entry) new-definition))
          ((not (eq (car (function-cell entry))
                    (unbound entry)))
           (error 'redefinition :name function-name
                                :oldtype :function :newtype :special-operator))
          ((not (null (macro-function entry)))
           (error 'redefinition :name function-name
                                :oldtype :macro :newtype :special-operator))
          (t
           (cl:setf (special-operator entry) new-definition)))))

(defmethod f1:fdefinition (function-name (env ford-environment))
 ;; (break "~A" function-name)
  (let ((entry (find-function-entry env function-name)))
    (cond ((null entry)
	   (break "~S" function-name)
           (error 'undefined-function :name function-name))
          ((not (eq (car (function-cell entry))
                    (unbound entry)))
           (car (function-cell entry)))
          ((not (null (macro-function entry)))
           `(cl:macro-function ,(macro-function entry)))
          ((not (null (special-operator entry)))
           `(cl:special (special-operator entry)))
          (t
	   (break "? ~S" function-name)
           (error 'undefined-function :name function-name)))))

(defmethod (cl:setf f1:fdefinition)
    (new-definition function-name (env ford-environment))
  (assert (functionp new-definition))

 ;; (break "? ~S ~S" function-name env)
  (let ((entry (ensure-function-entry env function-name)))
    (cond ((not (null (special-operator entry)))
           (error 'redefinition :name function-name
                                :oldtype :special-operator :newtype :function))
          (t
           (remove-function-name (car (function-cell entry)) function-name env)
           (cl:setf (car (function-cell entry)) new-definition)
           (add-function-name new-definition function-name env)
           (cl:setf (macro-function entry) nil)
           (cl:setf (type entry) t)
           (cl:setf (inline entry) nil)
           new-definition))))

(defmethod f1:macro-function (symbol (env ford-environment))
  (let ((entry (find-function-entry env symbol)))
    (if (null entry)
        nil
        (macro-function entry))))

(defmethod (cl:setf f1:macro-function)
    (new-definition function-name (env ford-environment))
  (assert (functionp new-definition))
  (let ((entry (ensure-function-entry env function-name)))
    (remove-function-name (car (function-cell entry)) function-name env)
    (cl:setf (car (function-cell entry)) (unbound entry))
    (cl:setf (macro-function entry) new-definition)
    (cl:setf (type entry) t)
    (cl:setf (inline entry) nil)
    new-definition))

(defmethod f1:compiler-macro-function
    (function-name (env ford-environment))
  (let ((entry (assoc function-name (compiler-macro-expanders env)
                      :test #'equal)))
    (if (null entry)
        nil
        (cdr entry))))

(defmethod (cl:setf f1:compiler-macro-function)
    (new-definition function-name (env ford-environment))
  (assert (or (functionp new-definition) (null new-definition)))
  (if (null new-definition)
      (cl:setf (compiler-macro-expanders env)
            (remove function-name (compiler-macro-expanders env)
                    :key #'car :test #'equal))
      (let ((entry (assoc function-name (compiler-macro-expanders env)
                          :test #'equal)))
        (if (null entry)
            (push (cons function-name new-definition)
                  (compiler-macro-expanders env))
            (cl:setf (cdr entry) new-definition))))
  new-definition)

(defmethod f1:function-type (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (if (or (null entry)
            (not (null (special-operator entry)))
            (not (null (macro-function entry))))
        nil
        (type entry))))

(defmethod (cl:setf f1:function-type)
    (new-type function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (cond ((not (null (special-operator entry)))
           (error 'attempt-to-proclaim-type-of-special-operator
                  :name  function-name))
          ((not (null (macro-function entry)))
           (error 'attempt-to-proclaim-type-of-macro
                  :name  function-name))
          (t
           (cl:setf (type entry) new-type)))))

(defmethod  f1:function-inline (function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (if (null entry)
        (error 'undefined-function :name function-name)
        (inline entry))))

(defmethod (cl:setf f1:function-inline)
    (new-inline function-name (env ford-environment))
  (assert (member new-inline '(nil cl:inline cl:notinline)))
  (let ((entry (find-function-entry env function-name)))
    (if (or (null entry)
            (eq (car (function-cell entry)) (unbound entry)))
        (error 'undefined-function :name function-name)
        (cl:setf (inline entry) new-inline))))

(defmethod f1:function-cell (function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (function-cell entry)))

(defmethod f1:function-unbound (function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (unbound entry)))

(defmethod f1:function-lambda-list
    (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (cond ((null entry)
           (values nil nil))
          ((lambda-list-valid-p entry)
           (values (lambda-list entry) t))
          (t
           (values nil nil)))))

(defmethod (cl:setf f1:function-lambda-list)
    (new-lambda-list function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (cl:setf (lambda-list entry) new-lambda-list)
    (cl:setf (lambda-list-valid-p entry) t)))

(defmethod f1:function-ast (function-name (env ford-environment))
  (let ((entry (find-function-entry env function-name)))
    (if (null entry)
        nil
        (ast entry))))

(defmethod (cl:setf f1:function-ast)
    (new-ast function-name (env ford-environment))
  (let ((entry (ensure-function-entry env function-name)))
    (cl:setf (ast entry) new-ast)))

;;; According to the protocol definition, the consequences are
;;; undefined if FUNCTION is not a function object.  We define the
;;; consequences here to be that an error is signaled.
(defmethod f1:function-names (function (env ford-environment))
  (assert (functionp function))
  (gethash function (function-names env)))

(defmethod f1:boundp (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))
    (and (not (null entry))
         (or (not (null (macro-function entry)))
             (not (eq (car (value-cell entry)) (unbound env)))))))

(defmethod f1:makunbound (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))
    (unless (null entry)
      (cl:setf (macro-function entry) nil)
      ;; Free up the expansion so that the garbage collector can
      ;; recycle it.
      (cl:setf (expansion entry) nil)
      (cl:setf (constantp entry) nil)
      (cl:setf (car (value-cell entry)) (unbound env)))))

(defmethod f1:constant-variable (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))
    (if (or (null entry) (not (constantp entry)))
        (values nil nil)
        (values (car (value-cell entry)) t))))

(defmethod (cl:setf f1:constant-variable)
    (value symbol (env ford-environment))
  (let ((entry (ensure-variable-entry env symbol)))
    (cond ((not (null (macro-function entry)))
           (error 'redefinition
                  :name symbol
                  :oldtype :symbol-macro :newtype :constant))
          ((specialp entry)
           (error 'redefinition
                  :name symbol
                  :oldtype :special-variable :newtype :constant))
          ((and (constantp entry)
                (not (eql (car (value-cell entry)) value)))
           (error 'constant-redefinition
                  :name symbol
                  :old (car (value-cell entry)) :new value))
          (t
           (cl:setf (constantp entry) t)
           (cl:setf (car (value-cell entry)) value)))))

(defmethod f1:special-variable (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))    
    (values (if (or (null entry)
                    (not (specialp entry))
                    (eq (car (value-cell entry)) (unbound env)))
                nil
                (car (value-cell entry)))
            (and (not (null entry)) (specialp entry)))))

(defmethod (cl:setf f1:special-variable)
    (value symbol (env ford-environment) initialize-p)

  (let ((entry (ensure-variable-entry env symbol)))
;;    (when (string-equal (format nil "~S" symbol) "YYY")	  (break "~S ~S ~S" symbol env value))
    (cond ((constantp entry)
           (error 'redefinition :name symbol
                  :oldtype :constant :newtype :special-variable))
          ((not (null (macro-function entry)))
           (error 'redefinition
                  :name symbol
                  :oldtype :symbol-macro :newtype :special-variable))
          (t
	   (cl:setf (specialp entry) t)
           (when initialize-p
             (cl:setf (car (value-cell entry)) value))))))

(defmethod f1:symbol-macro (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))
    (if (or (null entry) (null (macro-function entry)))
        (values nil nil)
        (values (macro-function entry) (expansion entry)))))

(defmethod (cl:setf f1:symbol-macro)
    (expansion symbol (env ford-environment))
  (let ((entry (ensure-variable-entry env symbol)))
    (cond ((specialp entry)
           (error 'redefinition
                  :name symbol
                  :oldtype :special-variable :newtype :symbol-macro))
          ((constantp entry)
           (error 'redefinition
                  :name symbol
                  :oldtype :constant :newtype :symbol-macro))
          (t
           (cl:setf (expansion entry) expansion)
           (cl:setf (macro-function entry)
                 (lambda (form environment)
                   (declare (ignore form environment))
                   expansion))))))

(defmethod f1:variable-type (symbol (env ford-environment))
  (let ((entry (find-variable-entry env symbol)))
    (if (null entry)
        t
        (type entry))))

(defmethod (cl:setf f1:variable-type)
    (new-type symbol (env ford-environment))
  (let ((entry (ensure-variable-entry env symbol)))
    (if (constantp entry)
        (error 'attempt-to-proclaim-type-of-constant :name symbol)
        (cl:setf (type entry) new-type))))

(defmethod f1:variable-cell (symbol (env ford-environment))
  (let ((entry (ensure-variable-entry env symbol)))
    (value-cell entry)))

(defmethod f1:find-class (symbol (env ford-environment))
  (gethash symbol (classes env)))

(defmethod f1:variable-unbound (symbol (env ford-environment))
  (declare (ignore symbol))
  (unbound env))

(defmethod (cl:setf f1:find-class)
    (new-class symbol (env ford-environment))
  (cl:setf (gethash symbol (classes env)) new-class)
  new-class)

(defmethod f1:setf-expander (symbol (env ford-environment))
  (cdr (assoc symbol (setf-expanders env) :test #'eq)))

(defmethod (cl:setf f1:setf-expander)
    (new-expander symbol (env ford-environment))
  (let ((association (assoc symbol (setf-expanders env) :test #'eq)))
    (if (null association)
        (push (cons symbol new-expander) (setf-expanders env))
        (cl:setf (cdr association) new-expander))))

(defmethod f1:default-setf-expander ((env ford-environment))
  (default-setf-expander env))

(defmethod (cl:setf f1:default-setf-expander)
    (new-expander (env ford-environment))
  (cl:setf (default-setf-expander env) new-expander))

(defmethod f1:type-expander (symbol (env ford-environment))
  (cdr (assoc symbol (type-expanders env) :test #'eq)))

(defmethod (cl:setf f1:type-expander)
    (new-expander symbol (env ford-environment))
  (let ((association (assoc symbol (type-expanders env) :test #'eq)))
    (if (null association)
        (push (cons symbol new-expander) (type-expanders env))
        (cl:setf (cdr association) new-expander))))

(defmethod f1:find-package (name (env ford-environment))
  (values (gethash name (packages env))))

(defmethod (cl:setf f1:find-package)
    (new-package name (env ford-environment))
  (unless (null (gethash name (packages env)))
    (error 'a-package-with-the-given-name-already-exists
           :name name))
  (cl:setf (gethash name (packages env)) new-package))

(defmethod f1:find-method-combination-template
    (symbol (env ford-environment))
  (gethash symbol (method-combination-templates env)))

(defmethod (cl:setf f1:find-method-combination-template)
    (new-template symbol (env ford-environment))
  (cl:setf (gethash symbol (method-combination-templates env)) new-template)
  new-template)

(defmethod f1:declaration (name (env ford-environment))
  (gethash name (declarations env)))

(defmethod (cl:setf f1:declaration) (value name (env ford-environment))
  (if (null value)
      (remhash name (declarations env))
      (cl:setf (gethash name (declarations env)) t)))

(defmethod f1:declarations ((env ford-environment))
  (loop for key being each hash-key of (declarations env)
        collect key))

(defmethod f1:symbol-plist (symbol (env ford-environment))
  (gethash symbol (property-lists env)))

(defmethod (cl:setf f1:symbol-plist)
    (new-plist symbol (env ford-environment))
  (cl:setf (gethash symbol (property-lists env)) new-plist))

(defmethod f1:structure-type (name (env ford-environment))
  (let ((entry (find-structure-entry env name)))
    (if entry (type entry) nil)))

(defmethod (cl:setf f1:structure-type) (type name (env ford-environment))
  (let ((entry (ensure-structure-entry env name)))
    (cl:setf (type entry) type)))

(defmethod f1:structure-size (name (env ford-environment))
  (let ((entry (find-structure-entry env name)))
    (if entry (size entry) nil)))

(defmethod (cl:setf f1:structure-size) (size name (env ford-environment))
  (let ((entry (ensure-structure-entry env name)))
    (cl:setf (size entry) size)))
