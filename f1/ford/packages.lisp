(cl:in-package #:common-lisp-user)

(defpackage #:ford
  (:use #:common-lisp)
  (:shadow #:type
           #:inline #:notinline
           #:constantp
           #:macro-function #:compiler-macro-function
           #:undefined-function)
  (:export #:ford-environment))
