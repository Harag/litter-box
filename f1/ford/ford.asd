(cl:in-package #:asdf-user)

(defsystem :ford
  :depends-on (:f1
	       :cleavir-compilation-policy)
  :serial t
  :components
  ((:file "packages")
   (:file "conditions")
   (:file "condition-reporters-english")
   (:file "environment")
   (:file "methods")))
