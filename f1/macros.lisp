(cl:in-package #:f1)

(cl:defmacro setf (&whole form &environment env place new-value-form &rest more-pairs)
  (cond ((null more-pairs)
	 (multiple-value-bind (variables
			       values
			       store-variables
			       writer-form
			       reader-form)
	     (f1:get-setf-expansion place env)
	   (declare (ignore reader-form))
	   `(let* ,(mapcar #'list variables values)
	      ;; Optimize a bit when there is only one store variable.
	      ,(if (= 1 (length store-variables))
		   `(let ((,(first store-variables) ,new-value-form))
		      ,writer-form)
		   `(multiple-value-bind ,store-variables
			,new-value-form
		      ,writer-form)))))
	((not (null (cdr more-pairs)))
	 `(progn (setf ,place ,new-value-form)
		 (setf ,@more-pairs)))
	(t
	 (error 'odd-number-of-arguments-to-setf :form form))))


;;TODO: put (f1:global-environment) bain macro-funxtion
(cl:defmacro defmacro (name lambda-list &body body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (f1:setf (f1:macro-function ',name (f1:global-environment))
           ,(cleavir-code-utilities:parse-macro name lambda-list body))))


(cl:defun defun-expander (name lambda-list body)
  (multiple-value-bind (declarations documentation forms)
      (cleavir-code-utilities:separate-function-body body)
    (let ((global-env-var (gensym))
	  (arg-type
	    (cleavir-code-utilities:lambda-list-type-specifier lambda-list)))
      `(progn
         (eval-when (:compile-toplevel)
           (let ((,global-env-var (f1:global-environment)))
             (setf (f1:function-type ',name ,global-env-var)
                   `(function ,',arg-type t))
             (setf (f1:function-lambda-list ',name ,global-env-var)
                   ',lambda-list)))
         (eval-when (:load-toplevel :execute)
	   (setf (f1:fdefinition ',name (f1:global-environment))
		 (lambda ,lambda-list
		   ,@declarations
		   ,@(if (null documentation)
			 '()
			 (list documentation))
		   (block ,(if (symbolp name) name (second name))
		     ,@forms)))
           (setf (f1:function-type ',name (f1:global-environment))
                 `(function ,',arg-type t))
           (setf (f1:function-lambda-list ',name (f1:global-environment))
                 ',lambda-list)
           ',name)))))

(cl:defmacro defun (name lambda-list &body body)
  (defun-expander name lambda-list body))
