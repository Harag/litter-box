(cl:in-package #:litter-box)

(defparameter *global-environment* nil)

(defclass client (trucler-reference:client)
  ())

;;Not creating/adding global-environment in initialize-instance :after
;;like sicl does because it causes closures that hang around if the repl
;;is stopep and started multiple times. Looks like its the shadowing macros
;;setf and its ilk that is hanging onto the closures, dont understand why
;;its a todo for another day.
(defmethod f1:global-environment (&optional lexical-environment)
  (if (null lexical-environment)
              *global-environment*
              (trucler:global-environment
               (f1:client *global-environment*) lexical-environment)))

(defun add-symbols% (environment package)
  (do-external-symbols (symbol (find-package package))
    (when (boundp symbol)
      (setf (f1:special-variable symbol environment t)
	    (symbol-value symbol)))
    (when (fboundp symbol)
      (cond ((special-operator-p symbol)
	     ;;	 (break "? ~S ~S" symbol use-symbol)
	     (setf (f1:special-operator symbol environment) t))
	    ((not (null (macro-function symbol)))
	     (setf (f1:macro-function symbol environment)
		   (macro-function symbol)))
	    (t
	     (setf (f1:fdefinition symbol environment)
		   (fdefinition symbol)))))
	
    (when (fboundp `(setf ,symbol))
      (setf (f1:fdefinition `(setf ,symbol) environment)
	    (fdefinition `(setf ,symbol))))))

;;TODO: Is "shadowing" package symbols with box-symbols really working 
;;correctly or even desired?
(defun add-package-to-environment (environment package)
  (do-symbols (symbol (find-package package))
    (let ((use-symbol symbol))

      (do-external-symbols (s-symbol (find-package '#:litter-shadow-conditionals))
	(when (equalp (symbol-name symbol) (symbol-name s-symbol))          
	  (setf use-symbol s-symbol)))
      
      (do-external-symbols (s-symbol (find-package '#:litter-shadow))
	(when (equalp (symbol-name symbol) (symbol-name s-symbol))
	  (setf use-symbol s-symbol)))
      
      (do-external-symbols (f1-symbol (find-package '#:f1))
	(when (equalp (symbol-name symbol) (symbol-name f1-symbol))
	  (setf use-symbol f1-symbol)))
      
      (when (boundp symbol)
	  (setf (f1:special-variable symbol environment t)
		(symbol-value use-symbol)))
      
      (when (fboundp symbol)
	  (cond ((special-operator-p symbol)
		 (setf (f1:special-operator symbol environment) t))
		((not (or (null (macro-function use-symbol))
			  (null (macro-function symbol))))
		 (setf (f1:macro-function
			symbol
			environment)
		       (macro-function use-symbol)))
		(t
		 (setf (f1:fdefinition symbol environment)
		       (fdefinition use-symbol)))))
      
      (when (fboundp `(setf ,symbol))
	(setf (f1:fdefinition `(setf ,symbol) environment)
			(fdefinition `(setf ,symbol)))))))

(defun translate-form (form env)
  (let* ((client (make-instance 'client))
         (cst (cst:cst-from-expression form))
         (ast (cleavir-cst-to-ast:cst-to-ast client cst env))
         (table (make-hash-table :test #'eq))
         (var (gensym)))
   
    (gather-lexicals ast table)
    
    `(lambda (,var)
       (declare (ignorable ,var))
       (let ,(loop for ast being each hash-key of table using (hash-value create-p)
                   when create-p collect (cleavir-ast:name ast))
         ,(translate-ast ast var '())))))

(defun evaluate-form (cst env)
  (let ((translated (translate-form cst env)))
    (format t "~S" translated)
    (funcall (compile nil translated) env)))

(defun prep-enviroment (&optional first-class-environment)
  (let ((f1-env (or first-class-environment
		    (make-instance 'ford::ford-environment
				   :client (make-instance 'client)))))

    (add-symbols% f1-env '#:litter-shadow)    
    (add-symbols% f1-env '#:litter-shadow-conditionals)    
    (add-symbols% f1-env '#:f1)
  
    (add-package-to-environment f1-env '#:common-lisp)
    
    ;;TODO: Shadowing quasiquote with elector version does not work...reader macro?
    ;;The shadowing macros are introducing "native" quasiquote so either have to
    ;;add code for supported implementations or replace quasiquote in shadowing macros
    ;;with eclector version.
    (setf (f1:macro-function 'sb-int:quasiquote f1-env)
	  (macro-function 'sb-int:quasiquote))
    
    (setf (f1:macro-function 'eclector.reader:quasiquote f1-env)
	  (macro-function 'eclector.reader:quasiquote))
    
    f1-env))

(defmacro with-environment (first-class-environment &body body)
  `(let ((*global-environment*
	  (or ,first-class-environment (prep-enviroment))))
    ,@body))

(defun repl ()

  (with-environment ()

    (f1:fmakunbound 'cadr *global-environment*)

    (loop do (format t "KITTY> ")
         (finish-output)
	 (print (evaluate-form (eclector.reader:read)
			       *global-environment*))
         (terpri))))

(defun eval-script (script &optional first-class-environment)

  (with-environment first-class-environment
    (let ((forms)
	  (results))
      (loop for form = (eclector.reader:read script)
	 while form
	 do (push form forms))

      (dolist (form (reverse forms))
	(push 
	 (evaluate-form form *global-environment*)
	 results))
      (values (first results) (rest results)))))
