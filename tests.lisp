(cl:in-package #:litter-box)

(defparameter param 1)

(setf param 2)

;;TODO:setf not working
(defvar var)

(setf var 2)

(defvar var 1)

(setf var 2)

(defun eish ()
  2)

(defmacro poo ()
  `(list 2 3 4))
  
(defmacro pee ()
  `(cadr (list (list ,@(poo)) 4 6 7)))



(cheat 'cadr (list 1 (list 2 3)))

(funcall 'car (list 1 (list 2 3)))

(funcall 'cadr (list 1 (list 2 3)))


(apply 'car (list 1 (list 2 3)))

(apply 'cadr (list 1 (list 2 3)))


(eval `(cadr (list 1 (list 2 3))))

(compile nil (lambda ()
	       (cadr (list 1 (list 2 3)))))

(funcall (compile nil (lambda ()
			(cadr (list 1 (list 2 3))))))

(read-from-string "'cadr")

(funcall (read-from-string "'cadr") (list 1 (list 2 3)) nil)

(read-from-string "(cadr (list 1 (list 2 3)))")

(eval (read-from-string "(cadr (list 1 (list 2 3)))"))

(defparameter cheat (lambda ()
		      (cadr (list 1 (list 2 3)))))

(defun cheat (func &rest args)
  (funcall func args))

(cheat 'cadr (list 1 (list 2 3)))

(defmacro cheat (func &rest args)
  `(funcall ,func ,@args))

(cheat 'cadr (list 1 (list 2 3)))

(defmacro shallow-cheat ()
  `(funcall 'cadr (list 1 (list 2 3))))

(defmacro deap-cheat ()
  `.(shallow-cheat))

(deap-cheat)

(let ((cadr (list 1)))
  cadr)

(defclass hangaas ()
  ((eish  :initarg :eish
	  :initform nil
	  :accessor eish)))
